# UCBL-VIDEO-IO PACKAGE
In this package, you will find:
 - The `VideoMeta` class containing video metadata (framerate, image shape)
 - The `VideoReader` class acting as a wrapper of the OpenCV2 video-reader, implementing python's `with` statement, and providing frame-by-frame read through a generator member.
 - The `VideoWriter` class acting as a wrapper of the OpenCV2 video-writer, implementing python's `with` statement, and providing frame-by-frame write through a consumer member.