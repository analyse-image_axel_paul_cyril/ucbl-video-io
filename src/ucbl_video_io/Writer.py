import cv2


class VideoWriter(object):
    def __init__(self, path, meta):
        self.path = path
        self.meta = meta
        self.vid = None

    def __enter__(self):
        if self.vid is not None:
            raise RuntimeError("Cannot open the same VideoWriter more than once at a time")

        fourcc = cv2.VideoWriter_fourcc(*'mp4v')

        self.vid = cv2.VideoWriter(self.path, fourcc, self.meta.frame_rate, self.meta.shape)

        return self

    def write(self, frame):
        if self.vid is None:
            raise RuntimeError("File not opened")
        self.vid.write(frame)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.vid.release()
        self.vid = None
