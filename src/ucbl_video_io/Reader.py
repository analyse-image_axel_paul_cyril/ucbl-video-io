import cv2
from .Meta import VideoMeta


class VideoReader(object):
    def __init__(self, path):
        self.path = path
        self.vid = None
        self.__meta = None

    def __enter__(self):
        if self.vid is not None:
            raise RuntimeError("Cannot open the same VideoReader more than once at a time")

        self.vid = cv2.VideoCapture(self.path)

        self.__meta = VideoMeta(int(self.vid.get(cv2.CAP_PROP_FPS)),
                                (int(self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)), int(self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT))))

        return self

    def read(self):
        if self.vid is None:
            raise RuntimeError("File not opened")

        while self.vid.isOpened():
            ret, frame = self.vid.read()
            if ret:
                yield frame
            else:
                break

    def get_meta(self):
        if self.__meta is None:
            raise RuntimeError("File not opened")
        return self.__meta

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.vid.release()
        self.vid = None
        self.__meta = None
