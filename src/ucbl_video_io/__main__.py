import sys, getopt
import cv2

from ucbl_video_io import VideoReader


def usage():
    print("Usage: \nTODO.")


def read_args():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hg:i:o:f:d", ["help", "input=", "output=", "frame="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    input_file_arg = None
    output_file_arg = None
    frame_arg = None

    for op, arg in opts:
        if op in ("-h", "--help"):
            usage()
            sys.exit()
        elif op == '-d':
            global _debug
            _debug = 1
        elif op in ("-i", "--input"):
            input_file_arg = arg
        elif op in ("-o", "--output"):
            output_file_arg = arg
        elif op in ("-f", "--frame"):
            frame_arg = arg

    return input_file_arg, output_file_arg, frame_arg, args


def main():
    if_arg, of_arg, frame_arg, filters_arg = read_args()

    if if_arg is None:
        raise AttributeError("Missing input (-i <input path>)/(--input=<input path>) argument")
    if of_arg is None:
        raise AttributeError("Missing output (-o <output path>)/(--output=<output path>) argument")
    if frame_arg is None:
        raise AttributeError("Missing output (-f <index>)/(--frame=<index>) argument")

    frame_index = int(frame_arg)
    counter = 0
    with VideoReader(if_arg) as input_file:
        for i, frame in enumerate(input_file.read()):
            if i == frame_index:
                cv2.imwrite(of_arg, frame)
                return
            counter += 1

    raise AttributeError("Frame {} is not in video {}, which contains {} frames".format(frame_index, if_arg, counter))


if __name__ == "__main__":
    # execute only if run as a script
    main()
